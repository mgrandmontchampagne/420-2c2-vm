#!/bin/bash
/usr/bin/date >> /var/log/2c2.log
echo 2023021201-dev > /usr/share/2c2/version

FILE=/home/etudiant/code/python/fichier.py
if [ ! -e $FILE ]
then
    wget https://gitlab.com/mgrandmontchampagne/420-2c2-vm/-/raw/main/fichier.py -O $FILE
    chown etudiant.etudiant $FILE
fi
if id -nG "etudiant" | grep -qw "adm"; then
  echo "L'utilisateur etudiant est déjà membre du groupe adm"
else
  echo "Ajout de l'utilisateur etudiant au groupe adm"
  usermod -aG adm etudiant
fi

# On efface les logs de connection à chaque démarrage
> /var/log/auth.log