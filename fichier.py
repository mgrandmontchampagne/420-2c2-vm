try:
    nom_fichier="employés.csv"
    f = open(nom_fichier, "r")
    
    print(f"Le fichier {nom_fichier} est ouvert! \n")
    
    for _ in range(0,5):
        print(f.readline().strip())
    
    f.close()
except: 
    print(f"Quelque chose s'est mal passé... \n Le fichier {nom_fichier} existe-il? 🧐")